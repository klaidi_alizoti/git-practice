
###List
-git init
-git add
-git commit -m "msg"
-git push origin <master>
-git diff
-git show
-git pull origin <master>
-git remote add origin <link.git>
-git checkout <branch>
-git checkout - b <branch>
-git branch -d <branch>
-mkdir <dirname>--> add a new directory in a bash console
-ls --> listing our files in a bash console
-cd <dirname> --> navigate through a directory
-mv <oldfile> <newfile> -->move or change a file
-cp <files> <location>
-rm -r <dir>--> delete a directory
-rm <file> --> delete a file
